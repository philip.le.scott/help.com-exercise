const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the `send <jsonString>` vorpal command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits tcpClient#writeTo
 * @emits cli#print
 */
function sendCustomJson(args, callback) {
  const { user, jsonString } = args;
  let obj;
  try {
    EventBus.emit(EVENTS.CLI.PRINT, jsonString);
    obj = JSON.parse(jsonString);
    EventBus.emit(EVENTS.TCP_CLIENT.WRITE_TO, [obj], user);
  } catch (err) {
    EventBus.emit(EVENTS.CLI.PRINT, 'Unable to parse provided string to usable JSON representation. Please check your JSON and try again.');
  }
  callback();
}

module.exports = {
  name: 'send <user> <jsonString> ',
  msg: 'Sends a custom JSON payload to the TCP service. Your JSON must be enclosed in single quotes (\') and valid.',
  fn: sendCustomJson,
};
