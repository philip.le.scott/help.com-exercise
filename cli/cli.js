const vorpal = require('vorpal')();
const { EVENTS } = require('../helpers/constants');
const { EventBus } = require('../helpers/eventBus');
const { isLoggedInForCommand } = require('./loggedInValidator');

const sendCountReqCmd = require('./commands/sendCountReq');
const sendTimeReqCmd = require('./commands/sendTimeReq');
const sendCustomJsonCmd = require('./commands/sendCustomJson');
const sendLoginCmd = require('./commands/sendLogin');
const sendLogoutCmd = require('./commands/sendLogout');
const toggleVerboseCmd = require('./commands/toggleVerbose');
const filterUserResponsesCmd = require('./commands/filterUserResponses');
const filterUserRepsonseClearCmd = require('./commands/filterUserResponseClear');

EventBus.on(EVENTS.CLI.PRINT, (printLine) => {
  vorpal.log(printLine);
});

vorpal
  .command(sendLoginCmd.name, sendLoginCmd.msg)
  .action(sendLoginCmd.fn);

vorpal
  .command(sendLogoutCmd.name, sendLogoutCmd.msg)
  .validate(isLoggedInForCommand)
  .action(sendLogoutCmd.fn);

vorpal
  .command(sendCountReqCmd.name, sendCountReqCmd.msg)
  .validate(isLoggedInForCommand)
  .action(sendCountReqCmd.fn);

vorpal
  .command(sendTimeReqCmd.name, sendTimeReqCmd.msg)
  .validate(isLoggedInForCommand)
  .action(sendTimeReqCmd.fn);

vorpal
  .command(sendCustomJsonCmd.name, sendCustomJsonCmd.msg)
  .validate(isLoggedInForCommand)
  .action(sendCustomJsonCmd.fn);

vorpal
  .command(toggleVerboseCmd.name, toggleVerboseCmd.msg)
  .action(toggleVerboseCmd.fn);

vorpal
  .command(filterUserResponsesCmd.name, filterUserResponsesCmd.msg)
  .validate(isLoggedInForCommand)
  .action(filterUserResponsesCmd.fn);

vorpal
  .command(filterUserRepsonseClearCmd.name, filterUserRepsonseClearCmd.msg)
  .action(filterUserRepsonseClearCmd.fn);

vorpal
  .delimiter('helpcom-tcp$')
  .show()
  .log('You must be logged in before any commands will return valid data. Use the \'login\' command to due so.')
  .exec('help');

module.exports = { CLI: vorpal };
