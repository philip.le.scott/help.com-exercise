const CONFIG = require('../../config/loader');

describe('The config loader should load without errors', () => {
  it('Should contain the NET object and keys', (done) => {
    expect(CONFIG.NET).toBeDefined();
    expect(CONFIG.NET.ADDR).toBeDefined();
    expect(CONFIG.NET.PORT).toBeDefined();
    done();
  });
});
