const { fn } = require('../../../cli/commands/sendCustomJson');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the WRITE_TO event to send the time request', () => {
  it('Should emit the WRITE_TO event on the EventBus with the correct JSON array to parse', (done) => {
    const jsonString = '{"request":"time","id":"abc"}';
    const payloadObject = JSON.parse(jsonString);
    const expectedEmitPayload = [payloadObject];
    const user = 'test';
    const callables = {
      cmdCallback() {},
      writeEventHandler() {},
      printEventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'writeEventHandler');
    spyOn(callables, 'printEventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.TCP_CLIENT.WRITE_TO, callables.writeEventHandler);
    EventBus.on(EVENTS.CLI.PRINT, callables.printEventHandler);

    fn({ user, jsonString }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.writeEventHandler).toHaveBeenCalledTimes(1);
    expect(callables.writeEventHandler).toHaveBeenCalledWith(expectedEmitPayload, user);
    expect(callables.printEventHandler).toHaveBeenCalledTimes(1);

    done();
  });

  it('Should emit the WRITE_TO event on the EventBus with the correct JSON array to parse', (done) => {
    const jsonString = '{"request":"time","id":"abc"}';
    const payloadObject = JSON.parse(jsonString);
    const expectedEmitPayload = [payloadObject];
    const user = 'test';
    const callables = {
      cmdCallback() {},
      writeEventHandler() {},
      printEventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'writeEventHandler');
    spyOn(callables, 'printEventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.TCP_CLIENT.WRITE_TO, callables.writeEventHandler);
    EventBus.on(EVENTS.CLI.PRINT, callables.printEventHandler);

    fn({ user, jsonString }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.writeEventHandler).toHaveBeenCalledTimes(1);
    expect(callables.writeEventHandler).toHaveBeenCalledWith(expectedEmitPayload, user);
    expect(callables.printEventHandler).toHaveBeenCalledTimes(1);

    done();
  });

  it('Should emit the pring event with a error message when invalid when invalid JSON string is provided', (done) => {
    const jsonString = '{"request":"time","id":"abc}';
    const user = 'test';
    const callables = {
      cmdCallback() {},
      printEventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'printEventHandler');
    EventBus.on(EVENTS.CLI.PRINT, callables.printEventHandler);

    fn({ user, jsonString }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.printEventHandler).toHaveBeenCalledTimes(2);
    done();
  });
});
