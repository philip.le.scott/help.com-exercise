const { fn } = require('../../../cli/commands/sendLogout');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the USER_LOGOUT event to destory a user connection', () => {
  it('Should emit the USER_LOGOUT event on the EventBus with the command provided user', (done) => {
    const user = 'test';
    const callables = {
      cmdCallback() {},
      eventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'eventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.USER_LOGOUT, callables.eventHandler);

    fn({ user }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledWith(user);

    done();
  });
});
