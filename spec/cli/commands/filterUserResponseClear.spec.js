const { fn } = require('../../../cli/commands/filterUserResponseClear');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the FILTER_USER_CLEAR event to clear the user to filter requests by', () => {
  it('Should emit the FILTER_USER_CLEAR event on the EventBus with the an empty string', (done) => {
    const callables = {
      cmdCallback() {},
      eventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'eventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.FILTER_USER_CLEAR, callables.eventHandler);

    fn({ }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledWith('');

    done();
  });
});
