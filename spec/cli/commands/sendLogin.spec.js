const { fn } = require('../../../cli/commands/sendLogin');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the USER_LOGIN event to create a user connection', () => {
  it('Should emit the USER_LOGIN event on the EventBus with the command provided user', (done) => {
    const user = 'test';
    const callables = {
      cmdCallback() {},
      eventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'eventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.USER_LOGIN, callables.eventHandler);

    fn({ user }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledWith(user);

    done();
  });
});
