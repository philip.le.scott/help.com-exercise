const { attachTcpEventEmitters } = require('../../net/tcpEvents');

describe('All events handlers should be attached to the socket for TCP events', () => {
  it('Should attach all required events to the passed socket instance', (done) => {
    const attachedEvents = ['error', 'timeout', 'end', 'connect', 'data'];
    const callables = {
      fakeSocket: {
        on: () => {},
      },
    };
    spyOn(callables.fakeSocket, 'on');
    attachTcpEventEmitters(callables.fakeSocket);
    expect(callables.fakeSocket.on).toHaveBeenCalledTimes(5);
    // Get all of the param arrays that were collected by the Spy
    const paramCallArr = callables.fakeSocket.on.calls.allArgs();
    // Loop over the params that were passed in on each .on() call.
    paramCallArr.forEach((callParams) => {
      const validParam = attachedEvents.includes(callParams[0]);
      expect(validParam).toEqual(true);
      expect(typeof callParams[1]).toEqual('function');
    });
    done();
  });
});
