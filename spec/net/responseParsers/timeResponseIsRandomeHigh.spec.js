const { timeReponseIsRandomHigh } = require('../../../net/responseParsers/timeResponseIsRandomHigh');

describe('JSON object parsers for TCP time req response evaluation', () => {
  it('Should return true if the object nested property random is above 30', (done) => {
    const testObject = { type: 'msg', msg: { random: 40 } };
    const result = timeReponseIsRandomHigh(testObject);
    expect(result).toEqual(true);
    done();
  });
  it('Should return false if the object nested property random is above below', (done) => {
    const testObject = { type: 'msg', msg: { random: 10 } };
    const result = timeReponseIsRandomHigh(testObject);
    expect(result).toEqual(false);
    done();
  });
});
