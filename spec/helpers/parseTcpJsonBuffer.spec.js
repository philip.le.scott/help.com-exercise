const { parseTcpJsonBuffer } = require('../../helpers/parseTcpJsonBuffer');

describe('Buffer to Object conversion', () => {
  const json = { test: 'value' };

  it('Should convert a buffer to a usable object structure', (done) => {
    const tcpReponse = `${JSON.stringify(json)}\n`;
    // Replicates a TCP response with the carriage line character.
    const buffer = Buffer.from(tcpReponse);
    const converted = parseTcpJsonBuffer(buffer);
    expect(converted.test).toEqual(json.test);
    done();
  });
  it('Should throw a catchable error if the buffer does not contain usable JSON', (done) => {
    // This will replicate a badly formatted repsonse from the socket service.
    const tcpReponse = '{"test": "value}\n';
    // Replicates a TCP response with the carriage line character.
    const buffer = Buffer.from(tcpReponse);
    try {
      parseTcpJsonBuffer(buffer);
    } catch (err) {
      expect(err).toBeDefined();
      expect(err.message).toEqual('Unexpected end of JSON input');
    }
    done();
  });
});
