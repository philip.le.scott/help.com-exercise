const { EventBus } = require('../helpers/eventBus');
const { parseTcpJsonBuffer } = require('../helpers/parseTcpJsonBuffer');
const { EVENTS } = require('../helpers/constants');

/**
 * This is a sequence module that adds events and the appropriate reactions to
 * those events based on the state triggers of a socket. The emit statements
 * are here as technically the Socket instance is emitting however all those
 * emits are setup within the invokation of this module.
 * @module attachTcpEventEmitters
 * @emits tcpClient#connected
 * @emits tcpClient#readError
 * @emits tcpClient#readSuccess
 * @emits tcpClient#writeError
 * @emits tcpClient#writeSuccess
 * @emits tcpClient#timeout
 * @emits tcpClient#socketError
 * @emits tcpClient#socketClosed
 */
const attachReadEventHandler = (tcpSocketClient, user) => {
  tcpSocketClient.on('data', (dataBuffer) => {
    let dataObj;
    try {
      dataObj = parseTcpJsonBuffer(dataBuffer);
      EventBus.emit(EVENTS.TCP_CLIENT.READ_SUCCESS, dataObj, user);
    } catch (err) {
      EventBus.emit(EVENTS.TCP_CLIENT.READ_ERROR, err, dataBuffer, user);
    }
  });
};

const attachErrorEventHandler = (tcpSocketClient, user) => {
  tcpSocketClient.on('error', (err) => {
    EventBus.emit(EVENTS.TCP_CLIENT.SOCKET_ERROR, err, user);
  });
};

const attachConnectedEventHandler = (tcpSocketClient, user) => {
  tcpSocketClient.on('connect', () => {
    EventBus.emit(EVENTS.TCP_CLIENT.CONNECTED, user);
  });
};

const attachClosedEventHandler = (tcpSocketClient, user) => {
  tcpSocketClient.on('end', () => {
    EventBus.emit(EVENTS.TCP_CLIENT.SOCKET_CLOSED, user);
  });
};

const attachTimeoutEventHandler = (tcpSocketClient, user) => {
  tcpSocketClient.on('timeout', () => {
    EventBus.emit(EVENTS.TCP_CLIENT.TIMEOUT, user);
  });
};

const attachTcpEventEmitters = (tcpSocketClient, user) => {
  attachErrorEventHandler(tcpSocketClient, user);
  attachTimeoutEventHandler(tcpSocketClient, user);
  attachClosedEventHandler(tcpSocketClient, user);
  attachConnectedEventHandler(tcpSocketClient, user);
  attachReadEventHandler(tcpSocketClient, user);
  return tcpSocketClient;
};

module.exports = {
  attachTcpEventEmitters,
};
