/**
 * This small helper simple attempts to destruct a Object matching a known
 * pattern and evaluate if the pattern matches and what the 'random' value of
 * wihtin the TCP time request response is.
 * @param  {Object} object
 * @return {Boolean}
 */
function timeReponseIsRandomHigh(object) {
  if (!object && !object.msg) { return false; }
  return (object.msg.random > 30);
}
module.exports = { timeReponseIsRandomHigh };
