/**
 * This is a small helper function that will attempt to convert an array of Objects
 * into a serialized string that can be sent over the wire. It takes an addition
 * param that notes if it should attach the normally required carriage return
 * characters ( \n ), if this is false then this will generate a single line
 * with all the Objects present, if true it will generate a standard message
 * format and each object will be on it's own line.
 * @param  {Array}   [dataObjArr=[{}]]
 * @param  {Boolean} [lineEndings=true]
 * @return {String} A UTF8 string representation of the Objects contained in the passed array.
 */
const formatTcpJsonString = (dataObjArr = [{}], lineEndings = true) => {
  const stringedObjects = dataObjArr.map((dataObj) => {
    if (dataObj) {
      let jsonString = JSON.stringify(dataObj);
      jsonString += (lineEndings) ? '\n' : '';
      return jsonString;
    }
    return '';
  });
  return stringedObjects.join('');
};

module.exports = {
  formatTcpJsonString,
};
