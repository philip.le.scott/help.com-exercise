module.exports = {
  ERROR_MSGS: {
    TCP_CLIENT: {
      NO_HANDLER: 'No initial connection handler was provided for TCP client creation',
      NO_SOCKET_CRT: 'No socket was provided to create TCP client.',
      NO_SOCKET_DESTRO: 'No socket was provided to create TCP client.',
    },
    SESSION_MAN: {
      INVALID_CLIENT: 'Invalid TCP client instance provided to session manager for session creation.',
      CLIENT_EXISTS: 'Client for provided users already exists.',
      CLIENT_MISSING: 'Client with provided user does not exist.',
    },
  },
  EVENTS: {
    TCP_CLIENT: {
      CHANNEL: 'tcpClient',
      CONNECTED: 'tcpClient#connected',
      READ_ERROR: 'tcpClient#readError',
      READ_SUCCESS: 'tcpClient#readSuccess',
      WRITE_TO: 'tcpClient#writeTo',
      WRITE_ERROR: 'tcpClient#writeError',
      WRITE_SUCCESS: 'tcpClient#writeSuccess',
      TIMEOUT: 'tcpClient#timeout',
      SOCKET_ERROR: 'tcpClient#socketError',
      SOCKET_CLOSED: 'tcpClient#socketClosed',
    },
    CLI: {
      TOGGLE_VERBOSE: 'cli#toggleVerbose',
      USER_LOGIN: 'cli#userLogin',
      USER_LOGOUT: 'cli#userLogout',
      PRINT: 'cli#print',
      FILTER_USER: 'cli#filterUserResponse',
      FILTER_USER_CLEAR: 'cli#filterUserResponseClear',
    },
  },
};
