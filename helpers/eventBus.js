const { EventEmitter } = require('events');

const EventBus = new EventEmitter();

/**
 * This module is simple a wrapper replacement for something such as rabbitmq
 * in larger situations. For the current scale of the project this will be used
 * to emit events from the tcpClient socket in a easier fashion than exporting
 * the socket itself which will interfere with reconnection calls and would
 * require the index file to be included in anything that needed data form the
 * socket.
 * @module EventBus
 * @type {EventEmitter}
 */
module.exports = { EventBus };
