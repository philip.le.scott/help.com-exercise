/**
 * This is a small helper function that will attempt to convert a buffer into a
 * usable JSON object.
 * @param  {Buffer} buffer
 * @return {Object|Array}
 */
const parseTcpJsonBuffer = (buffer) => {
  const dataStr = buffer.toString('utf8');
  return JSON.parse(dataStr.substring(0, dataStr.length - 1));
};

module.exports = {
  parseTcpJsonBuffer,
};
